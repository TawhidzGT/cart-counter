package com.example.cartcounter.DbLayer;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.cartcounter.Model.Cart;

@Database(entities = {Cart.class},version = 1) //DB class and version mentioned
public abstract class CartDatabase extends RoomDatabase {
    private static CartDatabase instance;

    public abstract CartDao cartDao();

    public static synchronized CartDatabase getInstance(Context context)
    {
        if(instance==null)
        {
            instance = Room.databaseBuilder(context.getApplicationContext(),CartDatabase.class,"cart_database")
                    .fallbackToDestructiveMigration() // Migration on changes in DB
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private CartDao cartDao;

        private PopulateDbAsyncTask(CartDatabase db) {
            cartDao = db.cartDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cartDao.insert(new Cart(1,"Shopping","",0,10));
            return null;
        }
    }

}
