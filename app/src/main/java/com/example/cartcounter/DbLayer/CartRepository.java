package com.example.cartcounter.DbLayer;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.cartcounter.Model.Cart;

import java.util.List;

public class CartRepository {
    private CartDao cartDao;
    private LiveData<List<Cart>> allCartItems;

    public CartRepository(Application application)
    {
        CartDatabase cartDatabase=CartDatabase.getInstance(application);
        cartDao= cartDatabase.cartDao();
        allCartItems=cartDao.getAllCartItems();
    }

    public void insert(Cart cart)
    {
        new InsertCartAsyncTask(cartDao).execute(cart);
    }

    public void update(Cart cart)
    {
        new UpdateCartAsyncTask(cartDao).execute(cart);
    }

    public LiveData<List<Cart>> getAllCartItems()
    {
        return allCartItems;
    }

    // Each crud operation of room runs in a background thread using asynctask
    private static class InsertCartAsyncTask extends AsyncTask<Cart,Void,Void>
    {
        CartDao cartDao;

        public InsertCartAsyncTask(CartDao cartDao)
        {
            this.cartDao=cartDao;
        }

        @Override
        protected Void doInBackground(Cart... cartItems) {
            cartDao.insert(cartItems[0]);
            return null;
        }
    }

    private static class UpdateCartAsyncTask extends AsyncTask<Cart,Void,Void>
    {
        CartDao cartDao;

        public UpdateCartAsyncTask(CartDao cartDao)
        {
            this.cartDao=cartDao;
        }

        @Override
        protected Void doInBackground(Cart... cartItems) {
            cartDao.update(cartItems[0]);
            return null;
        }
    }

}
