package com.example.cartcounter.DbLayer;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cartcounter.Model.Cart;

import java.util.List;

@Dao
public interface CartDao {
        @Insert
        void insert(Cart cart);

        @Update
        void update(Cart cart);

        @Query("SELECT * FROM cart_table")
        LiveData<List<Cart>> getAllCartItems();
}
