package com.example.cartcounter;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.cartcounter.Model.Cart;

import java.util.List;


public class LeftFragment extends Fragment {

    private LeftTabViewModel mViewModel;
    private TextView counter;
    static Cart currentCart = new Cart();
    private mainActivityViewModel mainActivityViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.left_fragment, container, false);

        mViewModel = ViewModelProviders.of(this).get(LeftTabViewModel.class);
        mainActivityViewModel = ViewModelProviders.of(this).get(mainActivityViewModel.class);

        Button buttonAdd = view.findViewById(R.id.btnclickAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.updateAddCart(mainActivityViewModel);
            }
        });

        Button buttonRmv = view.findViewById(R.id.btnclickRmv);
        buttonRmv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.updateRemoveCart(mainActivityViewModel);
            }
        });

        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.cart_layout, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_addcart);

        View actionView = menuItem.getActionView();
        counter = actionView.findViewById(R.id.notification_badge);

        mainActivityViewModel.getAllCartItems().observe(LeftFragment.this, new Observer<List<Cart>>() {
            @Override
            public void onChanged(List<Cart> carts) {
                if (carts == null || carts.isEmpty()) {
                    Log.d("val1", "1st time");
                } else {
                    if (carts.get(0).getQuantity() < 1) {
                        counter.setVisibility(View.GONE);
                    } else {
                        counter.setVisibility(View.VISIBLE);
                        counter.setText(String.valueOf(carts.get(0).getQuantity()));
                    }
                    currentCart = carts.get(0);
                }
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

}

     /*
        //counter.setText(String.valueOf(MainActivity.cart_count));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
    */

      /*   mainActivityViewModel.getCount().observe(LeftFragment.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer s) {
                if(s<1)
                {
                    counter.setVisibility(View.GONE);
                }
                else
                {
                    counter.setVisibility(View.VISIBLE);
                    counter.setText(String.valueOf(s));
                }
            }
        });*/
