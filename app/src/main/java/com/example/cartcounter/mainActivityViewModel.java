package com.example.cartcounter;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cartcounter.DbLayer.CartRepository;
import com.example.cartcounter.Model.Cart;

import java.util.List;

public class mainActivityViewModel extends AndroidViewModel {

    private CartRepository repository;
    private LiveData<List<Cart>> allCartItems;
    private MutableLiveData<Integer> mCount;

    public mainActivityViewModel(@NonNull Application application) {
        super(application);
        repository = new CartRepository(application);
        allCartItems = repository.getAllCartItems();
    }

    public MutableLiveData<Integer> getCount() {
        if (mCount == null) {
            mCount = new MutableLiveData<>();
            mCount.setValue(0);
        }
        return mCount;
    }

    public void setCount() {
        if (mCount.getValue() != null) {
            mCount.setValue(mCount.getValue() + 1);
        }
    }

    public void update(Cart cart) {
        repository.update(cart);
    }


    public LiveData<List<Cart>> getAllCartItems() {
        return allCartItems;
    }

}
