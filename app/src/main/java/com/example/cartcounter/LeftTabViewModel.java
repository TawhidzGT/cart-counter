package com.example.cartcounter;


import android.util.Log;

import androidx.lifecycle.ViewModel;

public class LeftTabViewModel extends ViewModel {
    mainActivityViewModel mainActivityViewModel;

    void updateAddCart(mainActivityViewModel mainActivityViewModel) {
        this.mainActivityViewModel = mainActivityViewModel;
        if (LeftFragment.currentCart == null) {
            Log.d("val1", "1st time");
        } else {
            LeftFragment.currentCart.setQuantity(LeftFragment.currentCart.getQuantity() + 1);
            mainActivityViewModel.update(LeftFragment.currentCart);
        }
    }

    void updateRemoveCart(mainActivityViewModel mainActivityViewModel) {
        this.mainActivityViewModel = mainActivityViewModel;

        if (LeftFragment.currentCart == null) {
            Log.d("val1", "1st time");
        } else {
            if (LeftFragment.currentCart.getQuantity() > 0) {
                LeftFragment.currentCart.setQuantity(LeftFragment.currentCart.getQuantity() - 1);
                mainActivityViewModel.update(LeftFragment.currentCart);
            }
        }
    }

}


    /*
    private MutableLiveData<String> mCountText = new MutableLiveData<>();

    public void setIndex(String index) {
        mCountText.setValue(index);

       private LiveData<String> mText = Transformations.map(mCountText, new Function<String, String>() {
       @Override
       public String apply(String input) {
           return input;
       }
    });

        LiveData<String> getText() {
        mCountText= (MutableLiveData<String>) mainActivityViewModel.getText();
        return mCountText;

    }
    }*/

